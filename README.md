There is people who works with multiple projects and wanted an easier function 
to track how much time he spent on each project. My idea of an application is 
that you can make multiple timers and start and stop them at any point of time. 
It’s for people who work with multiple projects and want to know how long they 
have worked with each project. This will make work much easier, and to track 
the time spent. 

The app will use google play accounts for login so everyone can save their 
timers and login on another phone or android device to get the timers, so it’s 
not stuck on a specific device but it will be strict to android devices for now.
There will also be an offline mode option for users who don’t want to log in 
but the drawback is that it will be stuck to that device. Firebase will be used 
for storing all the data. The timers will store data about then they were 
created and the dates then the timers started and stopped and the user will 
be able to add additional information for each start and stop date. 